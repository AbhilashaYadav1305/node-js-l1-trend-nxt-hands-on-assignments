const events = require('events');

var eventEmitter = new events.EventEmitter();

//First event
eventEmitter.on("one", () => {
  console.log('First Event!');
});
eventEmitter.emit('one');

//Second event
eventEmitter.on("two", () => {
  console.log("Second Event!");

});
eventEmitter.emit('two');

//Third event
eventEmitter.on("three", ()=>{
  console.log("Third Event!")
});
eventEmitter.emit('three');

var util = require("util"),
  http = require("http");
const fs = require("fs");

var options = {
  host: "www.google.com",
  port: 80,
  path: "/"
};

var content = "";

var req = http.request(options, function(res) {
  res.setEncoding("utf8");
  res.on("data", function(chunk) {
    content += chunk;
  });
  res.on("end", () => {
    fs.appendFile('contentFile.txt', content, function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Contents copied to file 'contentFile.txt'! Please check.");
      }
    });
  });
});
req.end();

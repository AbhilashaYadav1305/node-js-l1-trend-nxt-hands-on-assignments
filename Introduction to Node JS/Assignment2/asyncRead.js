var fs = require('fs');

//taking fileName from arguments passed
const fileName = process.argv[2];

//Asynchronous mode:-
fs.readFile(fileName, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  console.log(data);
});
console.log("This is written after the read is initiated asynchronously!!");

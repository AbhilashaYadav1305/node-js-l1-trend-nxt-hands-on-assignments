
//requiring fs modules
const fs = require('fs');

//taking directory from arguments passed
const directoryPath = process.argv[2];

//passsing directoryPath and callback function
fs.readdir(directoryPath, function (err, files) {
    //handling error
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    }
    //listing all files using forEach
    files.forEach(function (file) {
        console.log(file);
    });
});

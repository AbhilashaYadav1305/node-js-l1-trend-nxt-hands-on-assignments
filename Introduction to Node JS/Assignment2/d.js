const readline = require('readline');
var fs = require('fs');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var data='';

const readFrom = process.argv[2];
const writeInto = process.argv[3];

  var rstream = fs.createReadStream(readFrom);

  rstream.setEncoding('UTF8');

  rstream.on('data', function(chunk) {
     data += chunk;
  });

  rstream.on('end',function() {
   console.log("Data Transmission completed from- "+ readFrom);
   writeFunction();
});

rstream.on('error', function(err) {
 console.log(err.stack);
});

function writeFunction(){
var wstream = fs.createWriteStream(writeInto);

wstream.write(data,'UTF8');

// Mark the end of file
wstream.end();

// Handle stream events --> finish, and error
wstream.on('finish', function() {
   console.log("Write completed.Please check the file!!");
});

wstream.on('error', function(err) {
   console.log(err.stack);
});
}

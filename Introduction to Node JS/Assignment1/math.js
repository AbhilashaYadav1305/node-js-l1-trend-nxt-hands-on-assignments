var math = {
    add: function(num1, num2){
      console.log("Added value for "+num1+" and "+num2+" is: "+ (num1+num2));
    },
    sub: function(num1, num2){
      console.log("Subtracted value for "+num1+" and "+num2+" is: "+ (num1-num2));
    },
    mul: function(num1, num2){
      console.log("Multiplied value for "+num1+" and "+num2+" is: "+ (num1*num2));
    },
    div: function(num1, num2){
      console.log("Divided value for "+num1+" and "+num2+" is: "+ (num1/num2));
    }
};

module.exports = math
